�]q (}q(X   extendsqX    qX   nameqX   $levelsqX
   implementsqhX   classqX   ErrorHandlerqX
   visibilityq	X   privateq
X   docqXX  /**
 * A generic ErrorHandler for the PHP engine.
 *
 * Provides five bit fields that control how errors are handled:
 * - thrownErrors: errors thrown as \ErrorException
 * - loggedErrors: logged errors, when not @-silenced
 * - scopedErrors: errors thrown or logged with their local context
 * - tracedErrors: errors logged with their stack trace
 * - screamedErrors: never @-silenced errors
 *
 * Each error level can be logged by a dedicated PSR-3 logger object.
 * Screaming only applies to logging.
 * Throwing takes precedence over logging.
 * Uncaught exceptions are logged as E_ERROR.
 * E_DEPRECATED and E_USER_DEPRECATED levels never throw.
 * E_RECOVERABLE_ERROR and E_USER_ERROR levels always throw.
 * Non catchable errors that can be detected at shutdown time are logged when the scream bit field allows so.
 * As errors have a performance cost, repeated errors are all logged, so that the developer
 * can see them and weight them as more important to fix than others of the same level.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 *
 * @final
 */qX   staticqX   0qX   returnsqhX   argsq]qX   pathqX[   /home/enrage/Projects/lean-center/lean-center/vendor/symfony/error-handler/ErrorHandler.phpqX   kindqX   varqu}q(hhhX   $loggersqhhhhh	h
hhhhhhh]qhhhhu}q(hhhX   $thrownErrorsqhhhhh	h
hhhhhhh]qhhhhu}q(hhhX   $scopedErrorsqhhhhh	h
hhhhhhh]qhhhhu}q(hhhX   $tracedErrorsq hhhhh	h
hhhhhhh]q!hhhhu}q"(hhhX   $screamedErrorsq#hhhhh	h
hhhhhhh]q$hhhhu}q%(hhhX   $loggedErrorsq&hhhhh	h
hhhhhhh]q'hhhhu}q((hhhX   $traceReflectorq)hhhhh	h
hhhhhhh]q*hhhhu}q+(hhhX   $debugq,hhhhh	h
hhhhhhh]q-hhhhu}q.(hhhX   $isRecursiveq/hhhhh	h
hhhhhhh]q0hhhhu}q1(hhhX   $isRootq2hhhhh	h
hhhhhhh]q3hhhhu}q4(hhhX   $exceptionHandlerq5hhhhh	h
hhhhhhh]q6hhhhu}q7(hhhX   $bootstrappingLoggerq8hhhhh	h
hhhhhhh]q9hhhhu}q:(hhhX   $reservedMemoryq;hhhhh	h
hhhX   1q<hhh]q=hhhhu}q>(hhhX   $toStringExceptionq?hhhhh	h
hhhh<hhh]q@hhhhu}qA(hhhX   $silencedErrorCacheqBhhhhh	h
hhhh<hhh]qChhhhu}qD(hhhX   $silencedErrorCountqEhhhhh	h
hhhh<hhh]qFhhhhu}qG(hhhX	   $exitCodeqHhhhhh	h
hhhh<hhh]qI(]qJ(X   $handlerqKhe]qL(X   $replaceqMheehhhhu}qN(hhhX   registerqOhhhhh	X   publicqPhX/   /**
     * Registers the error handler.
     */qQhh<hhhhIhhhX   funcqRu}qS(hhhX   intqThhhhh	hPhX�   /**
     * Calls a function and turns any PHP error into \ErrorException.
     *
     * @return mixed What $function(...$arguments) returns
     *
     * @throws \ErrorException When $function(...$arguments) triggers a PHP error
     */qUhh<hX   mixedqVh]qW(]qX(X	   $functionqYhe]qZ(X
   $argumentsq[heehhhhRu}q\(hhhX   __constructq]hhhhh	hPhhhhhhh]q^(]q_(X   $bootstrappingLoggerq`he]qa(X   $debugqbheehhhhRu}qc(hhhX   setDefaultLoggerqdhhhhh	hPhXk  /**
     * Sets a logger to non assigned errors levels.
     *
     * @param LoggerInterface $logger  A PSR-3 logger to put as default for the given levels
     * @param array|int       $levels  An array map of E_* to LogLevel::* or an integer bit field of E_* constants
     * @param bool            $replace Whether to replace or not any existing logger
     */qehhhhh]qf(]qg(X   $loggerqhX   LoggerInterfaceqie]qj(X   $levelsqkX	   array|intqle]qm(X   $replaceqnX   boolqoeehhhhRu}qp(hhhX
   setLoggersqqhhhhh	hPhX�   /**
     * Sets a logger for each error level.
     *
     * @param array $loggers Error levels to [LoggerInterface|null, LogLevel::*] map
     *
     * @return array The previous map
     *
     * @throws \InvalidArgumentException
     */qrhhhX   arrayqsh]qt]qu(X   $loggersqvX   arrayqweahhhhRu}qx(hhhX   setExceptionHandlerqyhhhhh	hPhX�   /**
     * Sets a user exception handler.
     *
     * @param callable(\Throwable $e)|null $handler
     *
     * @return callable|null The previous exception handler
     */qzhhhX   callableq{h]q|]q}(X   $handlerq~heahhhhRu}q(hhhX   throwAtq�hhhhh	hPhX  /**
     * Sets the PHP error levels that throw an exception when a PHP error occurs.
     *
     * @param int  $levels  A bit field of E_* constants for thrown errors
     * @param bool $replace Replace or amend the previous value
     *
     * @return int The previous value
     */q�hhhX   intq�h]q�(]q�(X   $levelsq�X   intq�e]q�(X   $replaceq�X   boolq�eehhhhRu}q�(hhhX   scopeAtq�hhhhh	hPhX  /**
     * Sets the PHP error levels for which local variables are preserved.
     *
     * @param int  $levels  A bit field of E_* constants for scoped errors
     * @param bool $replace Replace or amend the previous value
     *
     * @return int The previous value
     */q�hhhX   intq�h]q�(]q�(X   $levelsq�X   intq�e]q�(X   $replaceq�X   boolq�eehhhhRu}q�(hhhX   traceAtq�hhhhh	hPhX  /**
     * Sets the PHP error levels for which the stack trace is preserved.
     *
     * @param int  $levels  A bit field of E_* constants for traced errors
     * @param bool $replace Replace or amend the previous value
     *
     * @return int The previous value
     */q�hhhX   intq�h]q�(]q�(X   $levelsq�X   intq�e]q�(X   $replaceq�X   boolq�eehhhhRu}q�(hhhX   screamAtq�hhhhh	hPhX
  /**
     * Sets the error levels where the @-operator is ignored.
     *
     * @param int  $levels  A bit field of E_* constants for screamed errors
     * @param bool $replace Replace or amend the previous value
     *
     * @return int The previous value
     */q�hhhX   intq�h]q�(]q�(X   $levelsq�X   intq�e]q�(X   $replaceq�X   boolq�eehhhhRu}q�(hhhX
   reRegisterq�hhhhh	h
hXI   /**
     * Re-registers as a PHP error handler if levels changed.
     */q�hhhhh]q�]q�(X   $prevq�heahhhhRu}q�(hhhX   handleErrorq�hhhhh	hPhX@  /**
     * Handles errors by filtering then logging them according to the configured bit fields.
     *
     * @return bool Returns false when no handling happens so that the PHP engine can handle the error itself
     *
     * @throws \ErrorException When $this->thrownErrors requests so
     *
     * @internal
     */q�hhhX   boolq�h]q�(]q�(X   $typeq�he]q�(X   $messageq�he]q�(X   $fileq�he]q�(X   $lineq�heehhhhRu}q�(hhhX   handleExceptionq�hhhhh	hPhXq   /**
     * Handles an exception by logging then forwarding it to another handler.
     *
     * @internal
     */q�hhhhh]q�]q�(X
   $exceptionq�heahhhhRu}q�(hhhX   renderExceptionq�hhhhh	hPhX�   /**
     * Shutdown registered function for handling PHP fatal errors.
     *
     * @param array|null $error An array as returned by error_get_last()
     *
     * @internal
     */q�hh<hhh]q�]q�(X   $errorq�X
   array|nullq�eahhhhRu}q�(hhhh�hhhhh	h
hX�   /**
     * Renders the given exception.
     *
     * As this method is mainly called during boot where nothing is yet available,
     * the output is always either HTML or CLI depending where PHP runs.
     */q�hhhhh]q�]q�(X
   $exceptionq�heahhhhRu}q�(hhhX   getErrorEnhancersq�hhhhh	X	   protectedq�hX�   /**
     * Override this method if you want to define more error enhancers.
     *
     * @return ErrorEnhancerInterface[]
     */q�hhhhh]q�hhhhRu}q�(hhhX
   cleanTraceq�hhhX   nullq�h	h
hX�   /**
     * Cleans the trace by removing function arguments and the frames added by the error handler and DebugClassLoader.
     */q�hhhhh]q�(]q�(X
   $backtraceq�he]q�(X   $typeq�he]q�(X   $fileq�he]q�(X   $lineq�he]q�(X   $throwq�heehhhhRu}q�(hhhX   class_existsq�hhhh�h	h
hX�   /**
     * Parse the error message by removing the anonymous class notation
     * and using the parent class instead if possible.
     */q�hh<hhh]q�]q�(X   $messageq�heahhhhRu}q�(hhhhhhhh�h	hPhhhhhhh]q�hhhhue.