�]q (}q(X   extendsqX    qX   nameqX   $levelsqX
   implementsqhX   classqX   ErrorHandlerqX
   visibilityq	X   privateq
X   docqX�  /**
 * A generic ErrorHandler for the PHP engine.
 *
 * Provides five bit fields that control how errors are handled:
 * - thrownErrors: errors thrown as \ErrorException
 * - loggedErrors: logged errors, when not @-silenced
 * - scopedErrors: errors thrown or logged with their local context
 * - tracedErrors: errors logged with their stack trace
 * - screamedErrors: never @-silenced errors
 *
 * Each error level can be logged by a dedicated PSR-3 logger object.
 * Screaming only applies to logging.
 * Throwing takes precedence over logging.
 * Uncaught exceptions are logged as E_ERROR.
 * E_DEPRECATED and E_USER_DEPRECATED levels never throw.
 * E_RECOVERABLE_ERROR and E_USER_ERROR levels always throw.
 * Non catchable errors that can be detected at shutdown time are logged when the scream bit field allows so.
 * As errors have a performance cost, repeated errors are all logged, so that the developer
 * can see them and weight them as more important to fix than others of the same level.
 *
 * @author Nicolas Grekas <p@tchwork.com>
 * @author Grégoire Pineau <lyrixx@lyrixx.info>
 *
 * @final since Symfony 4.3
 *
 * @deprecated since Symfony 4.4, use Symfony\Component\ErrorHandler\ErrorHandler instead.
 */qX   staticqX   0qX   returnsqhX   argsq]qX   pathqXS   /home/enrage/Projects/lean-center/lean-center/vendor/symfony/debug/ErrorHandler.phpqX   kindqX   varqu}q(hhhX   $loggersqhhhhh	h
hhhhhhh]qhhhhu}q(hhhX   $thrownErrorsqhhhhh	h
hhhhhhh]qhhhhu}q(hhhX   $scopedErrorsqhhhhh	h
hhhhhhh]qhhhhu}q(hhhX   $tracedErrorsq hhhhh	h
hhhhhhh]q!hhhhu}q"(hhhX   $screamedErrorsq#hhhhh	h
hhhhhhh]q$hhhhu}q%(hhhX   $loggedErrorsq&hhhhh	h
hhhhhhh]q'hhhhu}q((hhhX   $traceReflectorq)hhhhh	h
hhhhhhh]q*hhhhu}q+(hhhX   $isRecursiveq,hhhhh	h
hhhhhhh]q-hhhhu}q.(hhhX   $isRootq/hhhhh	h
hhhhhhh]q0hhhhu}q1(hhhX   $exceptionHandlerq2hhhhh	h
hhhhhhh]q3hhhhu}q4(hhhX   $bootstrappingLoggerq5hhhhh	h
hhhhhhh]q6hhhhu}q7(hhhX   $reservedMemoryq8hhhhh	h
hhhX   1q9hhh]q:hhhhu}q;(hhhX   $toStringExceptionq<hhhhh	h
hhhh9hhh]q=hhhhu}q>(hhhX   $silencedErrorCacheq?hhhhh	h
hhhh9hhh]q@hhhhu}qA(hhhX   $silencedErrorCountqBhhhhh	h
hhhh9hhh]qChhhhu}qD(hhhX	   $exitCodeqEhhhhh	h
hhhh9hhh]qF(]qG(X   $handlerqHX	   self|nullqIe]qJ(X   $replaceqKX   boolqLeehhhhu}qM(hhhX   registerqNhhhhh	X   publicqOhX�   /**
     * Registers the error handler.
     *
     * @param self|null $handler The handler to register
     * @param bool      $replace Whether to replace or not any existing handler
     *
     * @return self The registered error handler
     */qPhh9hX   selfqQhhFhhhX   funcqRu}qS(hhhX   __constructqThhhhh	hOhhhhhhh]qU]qV(X   $bootstrappingLoggerqWheahhhhRu}qX(hhhX   setDefaultLoggerqYhhhhh	hOhX  /**
     * Sets a logger to non assigned errors levels.
     *
     * @param array|int $levels  An array map of E_* to LogLevel::* or an integer bit field of E_* constants
     * @param bool      $replace Whether to replace or not any existing logger
     */qZhhhhh]q[(]q\(X   $loggerq]he]q^(X   $levelsq_X	   array|intq`e]qa(X   $replaceqbX   boolqceehhhhRu}qd(hhhX
   setLoggersqehhhhh	hOhX�   /**
     * Sets a logger for each error level.
     *
     * @param array $loggers Error levels to [LoggerInterface|null, LogLevel::*] map
     *
     * @return array The previous map
     *
     * @throws \InvalidArgumentException
     */qfhhhX   arrayqgh]qh]qi(X   $loggersqjX   arrayqkeahhhhRu}ql(hhhX   setExceptionHandlerqmhhhhh	hOhX�   /**
     * Sets a user exception handler.
     *
     * @param callable $handler A handler that will be called on Exception
     *
     * @return callable|null The previous exception handler
     */qnhhhX   callableqoh]qp]qq(X   $handlerqrX   callableqseahhhhRu}qt(hhhX   throwAtquhhhhh	hOhX  /**
     * Sets the PHP error levels that throw an exception when a PHP error occurs.
     *
     * @param int  $levels  A bit field of E_* constants for thrown errors
     * @param bool $replace Replace or amend the previous value
     *
     * @return int The previous value
     */qvhhhX   intqwh]qx(]qy(X   $levelsqzX   intq{e]q|(X   $replaceq}X   boolq~eehhhhRu}q(hhhX   scopeAtq�hhhhh	hOhX  /**
     * Sets the PHP error levels for which local variables are preserved.
     *
     * @param int  $levels  A bit field of E_* constants for scoped errors
     * @param bool $replace Replace or amend the previous value
     *
     * @return int The previous value
     */q�hhhX   intq�h]q�(]q�(X   $levelsq�X   intq�e]q�(X   $replaceq�X   boolq�eehhhhRu}q�(hhhX   traceAtq�hhhhh	hOhX  /**
     * Sets the PHP error levels for which the stack trace is preserved.
     *
     * @param int  $levels  A bit field of E_* constants for traced errors
     * @param bool $replace Replace or amend the previous value
     *
     * @return int The previous value
     */q�hhhX   intq�h]q�(]q�(X   $levelsq�X   intq�e]q�(X   $replaceq�X   boolq�eehhhhRu}q�(hhhX   screamAtq�hhhhh	hOhX
  /**
     * Sets the error levels where the @-operator is ignored.
     *
     * @param int  $levels  A bit field of E_* constants for screamed errors
     * @param bool $replace Replace or amend the previous value
     *
     * @return int The previous value
     */q�hhhX   intq�h]q�(]q�(X   $levelsq�X   intq�e]q�(X   $replaceq�X   boolq�eehhhhRu}q�(hhhX
   reRegisterq�hhhhh	h
hXI   /**
     * Re-registers as a PHP error handler if levels changed.
     */q�hhhhh]q�]q�(X   $prevq�heahhhhRu}q�(hhhX   handleErrorq�hhhhh	hOhX�  /**
     * Handles errors by filtering then logging them according to the configured bit fields.
     *
     * @param int    $type    One of the E_* constants
     * @param string $message
     * @param string $file
     * @param int    $line
     *
     * @return bool Returns false when no handling happens so that the PHP engine can handle the error itself
     *
     * @throws \ErrorException When $this->thrownErrors requests so
     *
     * @internal
     */q�hhhX   boolq�h]q�(]q�(X   $typeq�X   intq�e]q�(X   $messageq�X   stringq�e]q�(X   $fileq�X   stringq�e]q�(X   $lineq�X   intq�eehhhhRu}q�(hhhX   handleExceptionq�hhhhh	hOhX  /**
     * Handles an exception by logging then forwarding it to another handler.
     *
     * @param \Exception|\Throwable $exception An exception to handle
     * @param array                 $error     An array as returned by error_get_last()
     *
     * @internal
     */q�hhhhh]q�(]q�(X
   $exceptionq�he]q�(X   $errorq�X   arrayq�eehhhhRu}q�(hhhX   getFatalErrorHandlersq�hhhhh	hOhX�   /**
     * Shutdown registered function for handling PHP fatal errors.
     *
     * @param array $error An array as returned by error_get_last()
     *
     * @internal
     */q�hh9hhh]q�]q�(X   $errorq�X   arrayq�eahhhhRu}q�(hhhh�hhhhh	X	   protectedq�hX�   /**
     * Gets the fatal error handlers.
     *
     * Override this method if you want to define more fatal error handlers.
     *
     * @return FatalErrorHandlerInterface[] An array of FatalErrorHandlerInterface
     */q�hhhhh]q�hhhhRu}q�(hhhX
   cleanTraceq�hhhX   nullq�h	h
hX�   /**
     * Cleans the trace by removing function arguments and the frames added by the error handler and DebugClassLoader.
     */q�hhhhh]q�(]q�(X
   $backtraceq�he]q�(X   $typeq�he]q�(X   $fileq�he]q�(X   $lineq�he]q�(X   $throwq�heehhhhRu}q�(hhhhhhhh�h	hOhhhhhhh]q�hhhhue.