<header>
  <div class="container">
    <div id="top_slider">
      <img src="/img/_src/banner_testing.jpg" alt="Slider">
    </div>

    <section class="top_menu">
      <nav class="top_nav_left">
        <ul>
          <li><a href="/">Главная</a></li>
          <li><a href="/about">О проекте</a></li>
          <li><a href="/azbuka_lean">Азбука LEAN</a></li>
        </ul>
      </nav>
      <div class="consultant">
        <a href="http://openadvisors.ru" target="_blank"><img src="/img/_src/icon_oi.png" alt="Open Advisors"> Обратиться к консультанту</a>
      </div>
      <div id="logo">
        <a href="/"><img src="/img/_src/lean-center.svg" alt="Lean-center"></a>
      </div>
      <nav class="top_nav_right">
        <ul>
          <li><a href="/for_authors">Авторам</a></li>
          <li><a href="/for_partners">Партнерам</a></li>
          <li><a href="/for_sponsors">Спонсорам</a></li>
        </ul>
      </nav>
    </section>
  </div>
</header>
