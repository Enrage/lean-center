<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<title>@yield('title')</title>
	<meta name="keywords" content="@yield('keywords')">
	<meta name="description" content="@yield('description')">
	<meta name="viewport" content="width=device-width">
  <link rel="shortcut icon" href="/img/_src/lean-center-icon.png" type="image/png">
	<link rel="stylesheet" href="/css/styles.min.css">
	<link rel="stylesheet" href="/fonts/font_awesome.css">
</head>

<body>
	<div id="wrapper">
		@include('layouts.header')

		@include('layouts.category_menu')

		<main>
			<div class="container">
				<div class="main">

					@yield('content')

				</div>
				@include('layouts.sidebar')
			</div>
		</main>
	</div>

	@include('layouts.footer')
	<script src="/js/app.js"></script>
</body>
</html>
