@extends('layouts.app')

@section('title', 'Lean-center')

@section('content')

@if (isset($category_name))
<h3 class="page_title">Раздел: {{ $category_name }}</h3>
@endif

@if (!empty($posts))
<div class="posts">
  @foreach ($posts as $post)
  <article class="post">
    <a href="/post/{{ $post->id }}" class="post_img"><img src="/img/_src/articles/{{ $post->img_src }}" alt="{{ $post->title }}"></a>
    <div class="post_cat_block">
      <span class="post_cat">{{ $post->category_name }}</span>
      <span class="post_date">{{ $post->add_date }}</span>
    </div>
    <div class="post_text">
      <h2 class="post_title"><a href="/post/{{ $post->id }}">{{ $post->title }}</a></h2>
      <p class="post_desc">{{ $post->anons }}</p>
    </div>
  </article>
  @endforeach
</div>
{{ $posts->links() }}
@else
<div>Постов пока нет!</div>
@endif

@endsection
