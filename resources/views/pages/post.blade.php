@extends('layouts.app')

@section('title', $post->title)
@section('keywords', $post->keywords)
@section('description', $post->description)

@section('content')
<div class="post">
  <div class="content">
    <h1 class="post_title">{{ $post->title }}</h1>
    @if (isset($post->authors[0]))
      <ul class="post_authors">
      @foreach ($post->authors as $author)
        <li><a href="/author/{{ $author->id }}" target="_blank">
          <div class="author_img">
            <img src="{{ $author->author_img }}" alt="{{ $author->name }}">
          </div>
          <span>{{ $author->name }}</span>
        </a></li>
      @endforeach
      </ul>
    @endif
    {!! $post->text !!}
  </div>
</div>
@endsection
