@extends('layouts.app')

@section('title', 'Pages')

@section('content')
<div class="news">
  <div class="content">
    <h2 class="page_title">Новости Lean-center</h2>
    @foreach ($news as $new)
      <h3><a href="/news/{{ $new->id }}">{{ $new->title }}</a></h3>
      <p>{{ $new->date }}</p>
    @endforeach
  </div>
</div>
@endsection
