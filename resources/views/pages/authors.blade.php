@extends('layouts.app')

@section('title', 'Авторы портала Lean-center')
@section('keywords', 'Авторы портала Lean-center Keywords')
@section('description', 'Авторы портала Lean-center Description')

@section('content')
<div class="authors">
  <div class="content">
    <h2 class="page_title">Авторы Lean-center</h2>
    <ul>
      @foreach ($authors as $author)
      <li>
        <a href="/author/{{ $author->id }}" target="_blank">
          <div class="author_img">
            <img src="{{ $author->author_img }}" alt="{{ $author->name }}">
          </div>
          <h3 class="author_name">{{ $author->name }}</h3>
        </a>
      </li>
      @endforeach
    </ul>
  </div>
</div>
@endsection
