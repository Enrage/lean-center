<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
    // return view('welcome');
// });

Route::get('/', 'IndexController@index');

Route::get('/news_archive', 'NewController@index');


Route::get('/category/{category}', 'PostController@posts_by_category');

Route::get('/post/{post_id}', 'PostController@get_post');

Route::get('/authors', 'AuthorController@index');

Route::get('/{slug}', 'PageController@page');
