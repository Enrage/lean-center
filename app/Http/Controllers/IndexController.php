<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Post;

class IndexController extends Controller
{
  public function index() {
    $posts = Post::where('visible', '1')->orderBy('id', 'desc')->paginate(15);
    foreach ($posts as &$post) {
      $post->add_date = date('d.m.Y', $post->add_date);
    }
    return view('pages.index', compact('posts'));
  }
}