<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Post;
use App\Category;
use Illuminate\Http\Request;

class PostController extends Controller
{
  public function posts_by_category($category, Request $request) {
    $category_data = Category::where('alias', $category)->first();
    $posts = Category::find($category_data->id)->posts()->orderBy('id', 'desc')->paginate(15);
    $category_name = $category_data->name;

    foreach ($posts as &$post) {
      $post->add_date = date('d.m.Y', $post->add_date);
    }
    return view('pages.index', compact('posts', 'category_name'));
  }

  public function get_post($post_id, Request $request) {
    $post = Post::where('id', $post_id)->first();
    $post->add_date = date('d.m.Y', $post->add_date);

    foreach ($post->authors as &$author) {
      if (empty($author->author_img)) {
        $author->author_img = '/img/_src/authors/no_photo.png';
      } else {
        $author->author_img = '/img/_src/articles/' . $author->author_img;
      }
    }
    return view('pages.post', compact('post'));
  }
}
