<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;

class PageController extends Controller
{
    public function page($slug)
    {
        // dd($request->segment(count($request->segments())));
        $page = Page::where('page', $slug)->first();
        if (!empty($page)) {
            return view('pages.page', compact('page'));
        } else {
            return '404';
        }
    }
}
