<?php
namespace App\Providers;

use Illuminate\Support\{ServiceProvider, Facades\View};
use App\News;

class SidebarServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->sideBar();
    }

    public function sideBar() {
        $news = News::where('cat', 'news')->orderBy('date', 'desc')->limit(6)->get();
        foreach ($news as &$new) {
            $new->date = date('d.m.Y', $new->date);
        }

        View::composer('layouts.sidebar', function($view) use ($news) {
            $view->with('news', $news);
        });
    }
}
